<style type="text/css">
	.row * {
		box-sizing: border-box;
	}
	.kotak_judul {
		 border-bottom: 1px solid #fff; 
		 padding-bottom: 2px;
		 margin: 0;
	}
</style>

<h4 class="page-header">
Selamat Datang
<small>
Hai, <?php echo $this->data['u_name']; ?> Silahkan pilih menu disamping untuk mengoprasikan aplikasi
</small>
</h4>

<div class="row" style="margin: 0 -15px;">
	<div class="col-sm-12">
		<table class="table table-bordered">
			<?php for($i=1;$i<8;$i++) { ?>
			<tr class="text-center">
				<td height="50px"><div  style="padding-top:3px"><button class="btn btn-block btn-primary">1</button></div></td>
				<td><div  style="padding-top:3px"><button class="btn btn-block btn-primary">2</button></div></td>
				<td><div  style="padding-top:3px"><button class="btn btn-block btn-primary">3</button></div></td>
				<td><div  style="padding-top:3px"><button class="btn btn-block btn-primary">4</button></div></td>
				<td><div  style="padding-top:3px"><button class="btn btn-block btn-primary">5</button></div></td>
				<td><div  style="padding-top:3px"><button class="btn btn-block btn-primary">6</button></div></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>
