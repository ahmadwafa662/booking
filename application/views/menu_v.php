<!-- search form -->
<!-- /.search form -->

<ul class="sidebar-menu">
<li class="<?php 
	 $menu_home_arr= array('home', '');
	 if(in_array($this->uri->segment(1), $menu_home_arr)) {echo "active";}?>">
		<a href="<?php echo base_url(); ?>home">
			<img height="20" src="<?php echo base_url().'assets/theme_admin/img/home.png'; ?>"> <span>Beranda</span>
		</a>
</li>


<!-- Menu Booking -->
<li  class="treeview <?php 
	 $menu_trans_arr= array('holding_budget','entitas_budget', 'revisi_budget', 'transfer_budget');
	 if(in_array($this->uri->segment(2), $menu_trans_arr)) {echo "active";}?>">

	<a href="#">
		<img height="20" src="<?php echo base_url().'assets/theme_admin/img/calender.png'; ?>">
		<span>Booking</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu">
		<li class="<?php if ($this->uri->segment(2) == 'holding_budget') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/holding_budget"> <i class="fa fa-folder-open-o"></i> Holding Budget </a></li>

		<li class="<?php if ($this->uri->segment(1) == 'entitas_budget') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>entitas_budget"> <i class="fa fa-folder-open-o"></i> Entitas Budget </a></li>

		<li class="<?php if ($this->uri->segment(1) == 'revisi_budget') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>revisi_budget"> <i class="fa fa-folder-open-o"></i> Revisi Budget </a></li>
		
		<li class="<?php if ($this->uri->segment(1) == 'transfer_budget') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>transfer_budget"> <i class="fa fa-folder-open-o"></i> Transfer Budget </a></li>
	</ul>
</li>




<!-- Menu Cash Bank / Transaction -->
<li  class="treeview <?php 
	 $menu_trans_arr= array('in_transaction','out_transaction', 'transfer_bank', 'lain_lain');
	 if(in_array($this->uri->segment(1), $menu_trans_arr)) {echo "active";}?>">

	<a href="#">
		<img height="20" src="<?php echo base_url().'assets/theme_admin/img/transaksi.png'; ?>">
		<span>Cash/Bank Transaction</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu">
		<li class="<?php if ($this->uri->segment(1) == 'in_transaction') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>in_transaction"> <i class="fa fa-folder-open-o"></i> In Transaction </a></li>

		<li class="<?php if ($this->uri->segment(1) == 'out_transaction') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>out_transaction"> <i class="fa fa-folder-open-o"></i> Out Transaction </a></li>

		<li class="<?php if ($this->uri->segment(1) == 'transfer_bank') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>transfer_bank"> <i class="fa fa-folder-open-o"></i> Transfer Bank/Cash </a></li>
		
		<li class="<?php if ($this->uri->segment(1) == 'lain_lain') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>lain_lain"> <i class="fa fa-folder-open-o"></i> Lain-Lain </a></li>
	</ul>
</li>




<!-- Menu Report -->
<li  class="treeview <?php 
	 $menu_trans_arr= array('forcasting_report','listing_budget', 'comparison_budget', 'budget_perentitas','etc');
	 if(in_array($this->uri->segment(1), $menu_trans_arr)) {echo "active";}?>">

	<a href="#">
		<img height="20" src="<?php echo base_url().'assets/theme_admin/img/laporan.png'; ?>">
		<span>Report</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu">
		<li class="<?php if ($this->uri->segment(1) == 'forcasting_report') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>forcasting_report"> <i class="fa fa-folder-open-o"></i> Forcasting Report </a></li>

		<li class="<?php if ($this->uri->segment(1) == 'listing_budget') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>listing_budget"> <i class="fa fa-folder-open-o"></i> Listing Budget </a></li>

		<li class="<?php if ($this->uri->segment(1) == 'comparison_budget') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>comparison_budget"> <i class="fa fa-folder-open-o"></i> Comparison Budget Per Entitas</a></li>
		
		<li class="<?php if ($this->uri->segment(1) == 'budget_perentitas') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>budget_perentitas"> <i class="fa fa-folder-open-o"></i> Budget Per Entitas </a></li>
		
		<li class="<?php if ($this->uri->segment(1) == 'etc') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>etc"> <i class="fa fa-folder-open-o"></i> Lain-Lain </a></li>
	</ul>
</li>



<!-- Menu master data -->
<li  class="treeview <?php 
	 $menu_trans_arr= array('mstr_block','mstr_direction', 'mstr_level', 'mstr_location','mstr_property','mstr_tax_header','mstr_type','mstr_uom','mstr_tax_hdr');
	 if(in_array($this->uri->segment(2), $menu_trans_arr)) {echo "active";}?>">

	<a href="#">
		<img height="20" src="<?php echo base_url().'assets/theme_admin/img/data.png'; ?>">
		<span>Master Data</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
	<ul class="treeview-menu">
		<li class="<?php if ($this->uri->segment(2) == 'mstr_property') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_property"> <i class="fa fa-folder-open-o"></i> Master Property</a></li>
		<li class="<?php if ($this->uri->segment(2) == 'mstr_type') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_type"> <i class="fa fa-folder-open-o"></i> Master Unit Type</a></li>
		<li class="<?php if ($this->uri->segment(2) == 'mstr_level') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_level"> <i class="fa fa-folder-open-o"></i> Master Level</a></li>
		<li class="<?php if ($this->uri->segment(2) == 'mstr_block') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_block"> <i class="fa fa-folder-open-o"></i> Master Block</a></li>
		<li class="<?php if ($this->uri->segment(2) == 'mstr_location') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_location"> <i class="fa fa-folder-open-o"></i> Master Location</a></li>
		<li class="<?php if ($this->uri->segment(2) == 'mstr_direction') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_direction"> <i class="fa fa-folder-open-o"></i> Master Direction</a></li>
		<li class="<?php if ($this->uri->segment(2) == 'mstr_uom') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_uom"> <i class="fa fa-folder-open-o"></i> Master UOM Master</a></li>
		<li class="<?php if ($this->uri->segment(2) == 'mstr_tax_hdr') { echo 'active'; } ?>">  <a href="<?php echo base_url(); ?>tjs/mstr_tax_hdr"> <i class="fa fa-folder-open-o"></i> Master TAX Header</a></li>
	</ul>
</li>

</ul>`