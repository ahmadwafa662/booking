<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mstr_block extends AdminController {

	public function __construct() {
		parent::__construct();	
	}	
	
	public function index() {
		$this->data['judul_browser'] = 'Master';
		$this->data['judul_utama'] = 'Master';
		$this->data['judul_sub'] = 'Block';

		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
		$crud->set_table('mstr_block');
		$crud->set_subject('Block');
	
		$crud->fields('id','block_cd','descs', 'audit_date', 'audit_user');
		$crud->columns('block_cd','descs', 'audit_date', 'audit_user');

		// $crud->field_type('akun','dropdown',array('Aktiva' => 'Aktiva','Pasiva' => 'Pasiva'));
		// $crud->field_type('pemasukan','dropdown',array('Y' => 'Ya','N' => 'Tidak'));
		// $crud->field_type('pengeluaran','dropdown',array('Y' => 'Ya','N' => 'Tidak'));
		// $crud->field_type('aktif','dropdown',array('Y' => 'Ya','N' => 'Tidak'));
		// $crud->field_type('laba_rugi','dropdown',array(' ' => 'BLANK','PENDAPATAN' => 'PENDAPATAN', 'BIAYA' => 'BIAYA'));
		
		// $crud->required_fields('kd_aktiva','jns_trans', 'akun', 'pemasukan', 'pengeluaran', 'aktif');
		// $crud->display_as('jns_trans','Jenis Transaksi');
		// $this->db->_protect_identifiers = FALSE;


		// $crud->order_by('RIGHT(REPLICATE(0,2)+LEFT(kd_aktiva,1), 1) ASC, RIGHT(REPLICATE(1,6)+LEFT(kd_aktiva,5), 5)', 'ASC');
		// $crud->order_by('RIGHT(REPLICATE("0",), 1, 0) ASC, LPAD(kd_aktiva, 5, 1)', 'ASC');
		//$this->db->_protect_identifiers = TRUE;

		$crud->unset_read();
		//$crud->unset_add();
		$crud->unset_delete();
		$output = $crud->render();

		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		

		$this->load->view('default_v', $output);
		

	}

}
