<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mstr_type extends AdminController {

	public function __construct() {
		parent::__construct();	
	}	
	
	public function index() {
		$this->data['judul_browser'] = 'Master';
		$this->data['judul_utama'] = 'Master';
		$this->data['judul_sub'] = 'Master type';

		$this->output->set_template('gc');

		$this->load->library('grocery_CRUD');
		$crud = new grocery_CRUD();
		$crud->set_table('mstr_type');
		$crud->set_primary_key('id','mstr_type');

		$crud->set_subject('Master type');

		$crud->fields('type_cd', 'descs','audit_date','audit_user');

		// $crud->field_type('aktif','dropdown',array('Y' => 'Ya','N' => 'Tidak'));
		$crud->columns('type_cd','descs','audit_date','audit_user');
		//$crud->fields('ket');

	
		$crud->display_as('type_cd','Code');
		$crud->display_as('descs','Description');
		$crud->display_as('audit_date','Add Time');
		$crud->display_as('audit_user','Add Users');

		// $crud->required_fields('ket');

		$crud->unset_read();
		$output = $crud->render();

		$out['output'] = $this->data['judul_browser'];
		$this->load->section('judul_browser', 'default_v', $out);
		$out['output'] = $this->data['judul_utama'];
		$this->load->section('judul_utama', 'default_v', $out);
		$out['output'] = $this->data['judul_sub'];
		$this->load->section('judul_sub', 'default_v', $out);
		$out['output'] = $this->data['u_name'];
		$this->load->section('u_name', 'default_v', $out);

		$this->load->view('default_v', $output);
		

	}

}
