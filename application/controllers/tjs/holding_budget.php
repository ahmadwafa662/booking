<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Holding_budget extends AdminController {

	public function __construct() {
        parent::__construct();	
        $this->load->helper('fungsi');
		$this->load->model('pengeluaran_m');
		$this->load->model('pemasukan_m');
	}	
	
	public function index() {
		$this->data['judul_browser'] = 'Transaksi';
		$this->data['judul_utama'] = 'Transaksi';
		$this->data['judul_sub'] = 'Pengeluaran Kas Tunai';

		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/default/easyui.css';
		$this->data['css_files'][] = base_url() . 'assets/easyui/themes/icon.css';
		$this->data['js_files'][] = base_url() . 'assets/easyui/jquery.easyui.min.js';

		#include tanggal
		$this->data['css_files'][] = base_url() . 'assets/extra/bootstrap_date_time/css/bootstrap-datetimepicker.min.css';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/bootstrap-datetimepicker.min.js';
		$this->data['js_files'][] = base_url() . 'assets/extra/bootstrap_date_time/js/locales/bootstrap-datetimepicker.id.js';

		#include daterange
		$this->data['css_files'][] = base_url() . 'assets/theme_admin/css/daterangepicker/daterangepicker-bs3.css';
		$this->data['js_files'][] = base_url() . 'assets/theme_admin/js/plugins/daterangepicker/daterangepicker.js';

		//number_format
		$this->data['js_files'][] = base_url() . 'assets/extra/fungsi/number_format.js';

		$this->data['kas_id'] = $this->pengeluaran_m->get_data_kas();
		$this->data['akun_id'] = $this->pengeluaran_m->get_data_akun();

		$this->data['isi'] = $this->load->view('tjs/unit_list_v', $this->data, TRUE);
		$this->load->view('themes/layout_utama_v', $this->data);
	}

}
