/*
 Navicat Premium Data Transfer

 Source Server         : sql-server
 Source Server Type    : SQL Server
 Source Server Version : 11002100
 Source Host           : DESKTOP-M5NR9JE:1433
 Source Catalog        : erp
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 11002100
 File Encoding         : 65001

 Date: 15/08/2020 22:50:42
*/


-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[ci_sessions]') AND type IN ('U'))
	DROP TABLE [dbo].[ci_sessions]
GO

CREATE TABLE [dbo].[ci_sessions] (
  [session_id] varchar(40) COLLATE Latin1_General_CI_AS DEFAULT ((0)) NOT NULL,
  [ip_address] varchar(50) COLLATE Latin1_General_CI_AS DEFAULT ((0)) NOT NULL,
  [user_agent] varchar(120) COLLATE Latin1_General_CI_AS  NOT NULL,
  [last_activity] int DEFAULT ((0)) NOT NULL,
  [user_data] text COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[ci_sessions] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'7eddd9a6415f4ccfcc37fb0bd3ee8890', N'::1', N'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36', N'1597500729', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'd12a8808d1121e1869acc017b7634daa', N'::1', N'WhatsApp/2.20.195.17 A', N'1597500735', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'2991971832e944e678c1cd090e7d7f24', N'::1', N'Mozilla/5.0 (Linux; U; Android 9; en-us; Redmi 8 Build/PKQ1.190319.001) AppleWebKit/537.36 (KHTML, like Gecko) Version/4', N'1597500770', N'a:4:{s:9:"user_data";s:0:"";s:5:"login";b:1;s:6:"u_name";s:5:"admin";s:5:"level";s:5:"admin";}')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'd96649b97374aac55b9ca8990e7307b9', N'::1', N'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.3', N'1597500864', N'a:4:{s:9:"user_data";s:0:"";s:5:"login";b:1;s:6:"u_name";s:5:"admin";s:5:"level";s:5:"admin";}')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'c55e380c6b55e924ec9edda725c56c81', N'::1', N'WhatsApp/2.20.195.17 A', N'1597501183', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'ed0eb87eb916c3744b7d302d0fdf16dd', N'::1', N'WhatsApp/2.20.196.16 A', N'1597503699', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'2ad8bf6c337962c3497a4d04e6bf7506', N'::1', N'WhatsApp/2.20.195.17 A', N'1597501184', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'62782564e02f748709dc1328bdac18e8', N'::1', N'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.3', N'1597501273', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'0daeb60a8ec8fb5578114493f29653b7', N'::1', N'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.3', N'1597501273', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'484fe8c573f80f3e87a96fb6eca76a85', N'::1', N'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.3', N'1597501283', N'a:4:{s:9:"user_data";s:0:"";s:5:"login";b:1;s:6:"u_name";s:5:"admin";s:5:"level";s:5:"admin";}')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'1d16ed6c1ea6c7402fecaaaeb21d097c', N'::1', N'Mozilla/5.0 (Linux; Android 9; SM-G9550) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Mobile Safari/537.3', N'1597503663', N'a:4:{s:9:"user_data";s:0:"";s:5:"login";b:1;s:6:"u_name";s:5:"admin";s:5:"level";s:5:"admin";}')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'7ef8f30cc45d0b91519778c851e95a41', N'::1', N'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36', N'1597503667', N'a:4:{s:9:"user_data";s:0:"";s:5:"login";b:1;s:6:"u_name";s:5:"admin";s:5:"level";s:5:"admin";}')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'56783a62b950d0553fd61f2fd6688ff4', N'::1', N'WhatsApp/2.20.196.16 A', N'1597503700', N'')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'211f06b97ebc6c396dababb41fe037a3', N'::1', N'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36 Edg/', N'1597505926', N'a:4:{s:9:"user_data";s:0:"";s:5:"login";b:1;s:6:"u_name";s:5:"admin";s:5:"level";s:5:"admin";}')
GO

INSERT INTO [dbo].[ci_sessions] ([session_id], [ip_address], [user_agent], [last_activity], [user_data]) VALUES (N'b57cb3c6422d046faff8e73bd321472b', N'127.0.0.1', N'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', N'1597504994', N'a:4:{s:9:"user_data";s:0:"";s:5:"login";b:1;s:6:"u_name";s:2:"12";s:5:"level";s:6:"member";}')
GO


-- ----------------------------
-- Table structure for jns_akun
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jns_akun]') AND type IN ('U'))
	DROP TABLE [dbo].[jns_akun]
GO

CREATE TABLE [dbo].[jns_akun] (
  [kd_aktiva] varchar(5) COLLATE Latin1_General_CI_AS  NOT NULL,
  [jns_trans] varchar(50) COLLATE Latin1_General_CI_AS  NOT NULL,
  [akun] varchar(50) COLLATE Latin1_General_CI_AS DEFAULT NULL NULL,
  [laba_rugi] varchar(50) COLLATE Latin1_General_CI_AS DEFAULT '' NOT NULL,
  [pemasukan] varchar(50) COLLATE Latin1_General_CI_AS DEFAULT NULL NULL,
  [pengeluaran] varchar(50) COLLATE Latin1_General_CI_AS DEFAULT NULL NULL,
  [aktif] varchar(50) COLLATE Latin1_General_CI_AS  NOT NULL,
  [id] bigint  IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE [dbo].[jns_akun] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of jns_akun
-- ----------------------------
SET IDENTITY_INSERT [dbo].[jns_akun] ON
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A4', N'Piutang Usaha', N'Aktiva', N'', N'Y', N'Y', N'Y', N'1')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A5', N'Piutang Karyawan', N'Aktiva', N'', N'N', N'Y', N'N', N'2')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A6', N'Pinjaman Anggota', N'Aktiva', N'', NULL, NULL, N'Y', N'3')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A7', N'Piutang Anggota', N'Aktiva', N'', N'Y', N'Y', N'N', N'4')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A8', N'Persediaan Barang', N'Aktiva', N'', N'N', N'Y', N'Y', N'5')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A9', N'Biaya Dibayar Dimuka', N'Aktiva', N'', N'N', N'Y', N'Y', N'6')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A10', N'Perlengkapan Usaha', N'Aktiva', N'', N'N', N'Y', N'Y', N'7')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'C', N'Aktiva Tetap Berwujud', N'Aktiva', N'', NULL, NULL, N'Y', N'8')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'C1', N'Peralatan Kantor', N'Aktiva', N'', N'N', N'Y', N'Y', N'9')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'C2', N'Inventaris Kendaraan', N'Aktiva', N'', N'N', N'Y', N'Y', N'10')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'C3', N'Mesin', N'Aktiva', N'', N'N', N'Y', N'Y', N'11')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'C4', N'Aktiva Tetap Lainnya', N'Aktiva', N'', N'Y', N'N', N'Y', N'12')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'E', N'Modal Pribadi', N'Aktiva', N'', NULL, NULL, N'N', N'13')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'E1', N'Prive', N'Aktiva', N'', N'Y', N'Y', N'N', N'14')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'F', N'Utang', N'Pasiva', N'', NULL, NULL, N'Y', N'15')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'F1', N'Utang Usaha', N'Pasiva', N'', N'Y', N'Y', N'Y', N'16')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'K3', N'Pengeluaran Lainnya', N'Aktiva', N'', N'N', N'Y', N'N', N'17')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'F4', N'Simpanan Sukarela', N'Pasiva', N'', NULL, NULL, N'Y', N'18')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'F5', N'Utang Pajak', N'Pasiva', N'', N'Y', N'Y', N'Y', N'19')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'H', N'Utang Jangka Panjang', N'Pasiva', N'', NULL, NULL, N'Y', N'20')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'H1', N'Utang Bank', N'Pasiva', N'', N'Y', N'Y', N'Y', N'21')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'H2', N'Obligasi', N'Pasiva', N'', N'Y', N'Y', N'N', N'22')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'I', N'Modal', N'Pasiva', N'', NULL, NULL, N'Y', N'23')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'I1', N'Simpanan Pokok', N'Pasiva', N'', NULL, NULL, N'Y', N'24')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'I2', N'Simpanan Wajib', N'Pasiva', N'', NULL, NULL, N'Y', N'25')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'I3', N'Modal Awal', N'Pasiva', N'', N'Y', N'Y', N'Y', N'26')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'I4', N'Modal Penyertaan', N'Pasiva', N'', N'Y', N'Y', N'N', N'27')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'I5', N'Modal Sumbangan', N'Pasiva', N'', N'Y', N'Y', N'Y', N'28')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'I6', N'Modal Cadangan', N'Pasiva', N'', N'Y', N'Y', N'Y', N'29')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'J', N'Pendapatan', N'Pasiva', N'PENDAPATAN', NULL, NULL, N'Y', N'30')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'J1', N'Pembayaran Angsuran', N'Pasiva', N'', NULL, NULL, N'Y', N'31')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'J2', N'Pendapatan Lainnya', N'Pasiva', N'PENDAPATAN', N'Y', N'N', N'Y', N'32')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'K', N'Beban', N'Aktiva', N'', NULL, NULL, N'Y', N'33')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'K2', N'Beban Gaji Karyawan', N'Aktiva', N'BIAYA', N'N', N'Y', N'Y', N'34')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'K3', N'Biaya Listrik dan Air', N'Aktiva', N'BIAYA', N'N', N'Y', N'Y', N'35')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'K4', N'Biaya Transportasi', N'Aktiva', N'BIAYA', N'N', N'Y', N'Y', N'36')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'K10', N'Biaya Lainnya', N'Aktiva', N'BIAYA', N'N', N'Y', N'Y', N'37')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'TRF', N'Transfer Antar Kas', NULL, N'', NULL, NULL, N'N', N'38')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'A11', N'Permisalan', N'Aktiva', N'', N'Y', N'Y', N'Y', N'39')
GO

INSERT INTO [dbo].[jns_akun] ([kd_aktiva], [jns_trans], [akun], [laba_rugi], [pemasukan], [pengeluaran], [aktif], [id]) VALUES (N'aaa', N'tesing data', N'Aktiva', N'Y', N'Y', N'N', N'Y', N'41')
GO

SET IDENTITY_INSERT [dbo].[jns_akun] OFF
GO


-- ----------------------------
-- Table structure for jns_angsuran
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jns_angsuran]') AND type IN ('U'))
	DROP TABLE [dbo].[jns_angsuran]
GO

CREATE TABLE [dbo].[jns_angsuran] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [ket] int  NOT NULL,
  [aktif] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[jns_angsuran] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of jns_angsuran
-- ----------------------------
SET IDENTITY_INSERT [dbo].[jns_angsuran] ON
GO

INSERT INTO [dbo].[jns_angsuran] ([id], [ket], [aktif]) VALUES (N'1', N'3', N'Y')
GO

INSERT INTO [dbo].[jns_angsuran] ([id], [ket], [aktif]) VALUES (N'2', N'6', N'Y')
GO

INSERT INTO [dbo].[jns_angsuran] ([id], [ket], [aktif]) VALUES (N'3', N'18', N'Y')
GO

INSERT INTO [dbo].[jns_angsuran] ([id], [ket], [aktif]) VALUES (N'4', N'24', N'Y')
GO

INSERT INTO [dbo].[jns_angsuran] ([id], [ket], [aktif]) VALUES (N'5', N'36', N'Y')
GO

INSERT INTO [dbo].[jns_angsuran] ([id], [ket], [aktif]) VALUES (N'6', N'5', N'Y')
GO

INSERT INTO [dbo].[jns_angsuran] ([id], [ket], [aktif]) VALUES (N'8', N'12', N'Y')
GO

SET IDENTITY_INSERT [dbo].[jns_angsuran] OFF
GO


-- ----------------------------
-- Table structure for jns_simpan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jns_simpan]') AND type IN ('U'))
	DROP TABLE [dbo].[jns_simpan]
GO

CREATE TABLE [dbo].[jns_simpan] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [jns_simpan] varchar(30) COLLATE Latin1_General_CI_AS  NOT NULL,
  [jumlah] float(53)  NOT NULL,
  [tampil] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[jns_simpan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of jns_simpan
-- ----------------------------
SET IDENTITY_INSERT [dbo].[jns_simpan] ON
GO

INSERT INTO [dbo].[jns_simpan] ([id], [jns_simpan], [jumlah], [tampil]) VALUES (N'1', N'Simpanan Sukarela', N'200000', N'Y')
GO

INSERT INTO [dbo].[jns_simpan] ([id], [jns_simpan], [jumlah], [tampil]) VALUES (N'2', N'Simpanan Pokok', N'100000', N'Y')
GO

INSERT INTO [dbo].[jns_simpan] ([id], [jns_simpan], [jumlah], [tampil]) VALUES (N'3', N'Simpanan Wajib', N'50000', N'Y')
GO

INSERT INTO [dbo].[jns_simpan] ([id], [jns_simpan], [jumlah], [tampil]) VALUES (N'14', N'simpanan suka suka', N'4000000', N'Y')
GO

INSERT INTO [dbo].[jns_simpan] ([id], [jns_simpan], [jumlah], [tampil]) VALUES (N'15', N'simpanan aja', N'800000', N'Y')
GO

INSERT INTO [dbo].[jns_simpan] ([id], [jns_simpan], [jumlah], [tampil]) VALUES (N'16', N'simpan simpan', N'0', N'Y')
GO

INSERT INTO [dbo].[jns_simpan] ([id], [jns_simpan], [jumlah], [tampil]) VALUES (N'17', N'coba', N'100000', N'Y')
GO

SET IDENTITY_INSERT [dbo].[jns_simpan] OFF
GO


-- ----------------------------
-- Table structure for nama_kas_tbl
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[nama_kas_tbl]') AND type IN ('U'))
	DROP TABLE [dbo].[nama_kas_tbl]
GO

CREATE TABLE [dbo].[nama_kas_tbl] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [nama] varchar(225) COLLATE Latin1_General_CI_AS  NOT NULL,
  [aktif] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmpl_simpan] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmpl_penarikan] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmpl_pinjaman] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmpl_bayar] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmpl_pemasukan] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmpl_pengeluaran] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmpl_transfer] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[nama_kas_tbl] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of nama_kas_tbl
-- ----------------------------
SET IDENTITY_INSERT [dbo].[nama_kas_tbl] ON
GO

INSERT INTO [dbo].[nama_kas_tbl] ([id], [nama], [aktif], [tmpl_simpan], [tmpl_penarikan], [tmpl_pinjaman], [tmpl_bayar], [tmpl_pemasukan], [tmpl_pengeluaran], [tmpl_transfer]) VALUES (N'1', N'Kas Tunai', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y')
GO

INSERT INTO [dbo].[nama_kas_tbl] ([id], [nama], [aktif], [tmpl_simpan], [tmpl_penarikan], [tmpl_pinjaman], [tmpl_bayar], [tmpl_pemasukan], [tmpl_pengeluaran], [tmpl_transfer]) VALUES (N'2', N'Kas Besar', N'Y', N'T', N'T', N'Y', N'T', N'Y', N'Y', N'Y')
GO

INSERT INTO [dbo].[nama_kas_tbl] ([id], [nama], [aktif], [tmpl_simpan], [tmpl_penarikan], [tmpl_pinjaman], [tmpl_bayar], [tmpl_pemasukan], [tmpl_pengeluaran], [tmpl_transfer]) VALUES (N'3', N'Bank BNI', N'Y', N'T', N'T', N'T', N'T', N'Y', N'Y', N'Y')
GO

INSERT INTO [dbo].[nama_kas_tbl] ([id], [nama], [aktif], [tmpl_simpan], [tmpl_penarikan], [tmpl_pinjaman], [tmpl_bayar], [tmpl_pemasukan], [tmpl_pengeluaran], [tmpl_transfer]) VALUES (N'7', N'kas BCA', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y', N'Y')
GO

SET IDENTITY_INSERT [dbo].[nama_kas_tbl] OFF
GO


-- ----------------------------
-- Table structure for pekerjaan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[pekerjaan]') AND type IN ('U'))
	DROP TABLE [dbo].[pekerjaan]
GO

CREATE TABLE [dbo].[pekerjaan] (
  [id_kerja] varchar(5) COLLATE Latin1_General_CI_AS  NOT NULL,
  [jenis_kerja] varchar(30) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[pekerjaan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of pekerjaan
-- ----------------------------
INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'1', N'TNI')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'2', N'PNS')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'3', N'Karyawan Swasta')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'4', N'Guru')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'5', N'Buruh')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'6', N'Tani')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'7', N'Pedagang')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'8', N'Wiraswasta')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'9', N'Mengurus Rumah Tangga')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'99', N'Lainnya')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'98', N'Pensiunan')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'97', N'Penjahit')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'1', N'TNI')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'2', N'PNS')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'3', N'Karyawan Swasta')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'4', N'Guru')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'5', N'Buruh')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'6', N'Tani')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'7', N'Pedagang')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'8', N'Wiraswasta')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'9', N'Mengurus Rumah Tangga')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'99', N'Lainnya')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'98', N'Pensiunan')
GO

INSERT INTO [dbo].[pekerjaan] ([id_kerja], [jenis_kerja]) VALUES (N'97', N'Penjahit')
GO


-- ----------------------------
-- Table structure for suku_bunga
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[suku_bunga]') AND type IN ('U'))
	DROP TABLE [dbo].[suku_bunga]
GO

CREATE TABLE [dbo].[suku_bunga] (
  [opsi_key] varchar(20) COLLATE Latin1_General_CI_AS  NOT NULL,
  [opsi_val] varchar(255) COLLATE Latin1_General_CI_AS DEFAULT NULL NULL,
  [id] int  IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE [dbo].[suku_bunga] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of suku_bunga
-- ----------------------------
SET IDENTITY_INSERT [dbo].[suku_bunga] ON
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'bg_tab', N'0', N'1')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'bg_pinjam', N'2', N'2')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'biaya_adm', N'1500', N'3')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'denda', N'1000', N'4')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'denda_hari', N'15', N'5')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'dana_cadangan', N'40', N'6')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'jasa_anggota', N'40', N'7')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'dana_pengurus', N'5', N'8')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'dana_karyawan', N'5', N'9')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'dana_pend', N'5', N'10')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'dana_sosial', N'5', N'11')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'jasa_usaha', N'70', N'12')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'jasa_modal', N'30', N'13')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'pjk_pph', N'5', N'14')
GO

INSERT INTO [dbo].[suku_bunga] ([opsi_key], [opsi_val], [id]) VALUES (N'pinjaman_bunga_tipe', N'A', N'15')
GO

SET IDENTITY_INSERT [dbo].[suku_bunga] OFF
GO


-- ----------------------------
-- Table structure for tbl_anggota
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_anggota]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_anggota]
GO

CREATE TABLE [dbo].[tbl_anggota] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [nama] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [identitas] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [jk] varchar(1) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tmp_lahir] varchar(225) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tgl_lahir] date  NOT NULL,
  [status] varchar(30) COLLATE Latin1_General_CI_AS  NOT NULL,
  [agama] varchar(30) COLLATE Latin1_General_CI_AS  NOT NULL,
  [departement] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [pekerjaan] varchar(30) COLLATE Latin1_General_CI_AS  NOT NULL,
  [alamat] varchar(max) COLLATE Latin1_General_CI_AS  NOT NULL,
  [kota] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [notelp] varchar(12) COLLATE Latin1_General_CI_AS  NOT NULL,
  [jabatan_id] int  NOT NULL,
  [aktif] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [pass_word] varchar(225) COLLATE Latin1_General_CI_AS  NOT NULL,
  [file_pic] varchar(225) COLLATE Latin1_General_CI_AS  NOT NULL,
  [tgl_daftar] date  NULL
)
GO

ALTER TABLE [dbo].[tbl_anggota] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_anggota
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_anggota] ON
GO

INSERT INTO [dbo].[tbl_anggota] ([id], [nama], [identitas], [jk], [tmp_lahir], [tgl_lahir], [status], [agama], [departement], [pekerjaan], [alamat], [kota], [notelp], [jabatan_id], [aktif], [pass_word], [file_pic], [tgl_daftar]) VALUES (N'1', N'anggota', N'anggota', N'L', N'kediri', N'1996-08-08', N'Belum Kawin', N'', N'Produksi Slitting', N'', N'kediri', N'kediri', N'', N'2', N'Y', N'0bee337b1f1e9a9e417decc70d61b0d4bfca50fd', N'94737-logo192.png', N'2020-08-15')
GO

INSERT INTO [dbo].[tbl_anggota] ([id], [nama], [identitas], [jk], [tmp_lahir], [tgl_lahir], [status], [agama], [departement], [pekerjaan], [alamat], [kota], [notelp], [jabatan_id], [aktif], [pass_word], [file_pic], [tgl_daftar]) VALUES (N'12', N'Andi P', N'andi', N'L', N'Jakarta', N'1992-03-20', N'Belum Kawin', N'Islam', N'Produksi BOPP', N'TNI', N'JL RAYA HAYAM WURUK KDR', N'KEDIRI', N'085677883377', N'2', N'Y', N'1e157dd5081c6699192c94068932336f5c00ebf5', N'6728b-awmleer-i--yyrxuphc-unsplash.jpg', N'2020-08-15')
GO

INSERT INTO [dbo].[tbl_anggota] ([id], [nama], [identitas], [jk], [tmp_lahir], [tgl_lahir], [status], [agama], [departement], [pekerjaan], [alamat], [kota], [notelp], [jabatan_id], [aktif], [pass_word], [file_pic], [tgl_daftar]) VALUES (N'13', N'ario', N'ariobiesch', N'L', N'manado', N'1980-08-09', N'Kawin', N'Protestan', N'Accounting', N'Wiraswasta', N'jln nakula 3c', N'bekasi', N'081311876141', N'2', N'Y', N'1e157dd5081c6699192c94068932336f5c00ebf5', N'651f9-artistic-green-background-wallpaper-1920x1200-11056_6.jpg', N'2020-08-05')
GO

SET IDENTITY_INSERT [dbo].[tbl_anggota] OFF
GO


-- ----------------------------
-- Table structure for tbl_barang
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_barang]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_barang]
GO

CREATE TABLE [dbo].[tbl_barang] (
  [nm_barang] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [type] varchar(50) COLLATE Latin1_General_CI_AS  NOT NULL,
  [merk] varchar(50) COLLATE Latin1_General_CI_AS  NOT NULL,
  [harga] float(53)  NOT NULL,
  [jml_brg] int  NOT NULL,
  [ket] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [id] bigint  IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_barang] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_barang
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_barang] ON
GO

INSERT INTO [dbo].[tbl_barang] ([nm_barang], [type], [merk], [harga], [jml_brg], [ket], [id]) VALUES (N'Lemari Es', N'Elektronik', N'Toshiba', N'500000', N'4', N'', N'1')
GO

INSERT INTO [dbo].[tbl_barang] ([nm_barang], [type], [merk], [harga], [jml_brg], [ket], [id]) VALUES (N'Komputer', N'K300 Corei3', N'Asus', N'5000000', N'3', N'', N'2')
GO

INSERT INTO [dbo].[tbl_barang] ([nm_barang], [type], [merk], [harga], [jml_brg], [ket], [id]) VALUES (N'Kompor Gas', N'Tr675000', N'Rinai', N'100000', N'6', N'', N'3')
GO

INSERT INTO [dbo].[tbl_barang] ([nm_barang], [type], [merk], [harga], [jml_brg], [ket], [id]) VALUES (N'Pinjaman Uang', N'Uang', N'-', N'0', N'0', N'', N'4')
GO

INSERT INTO [dbo].[tbl_barang] ([nm_barang], [type], [merk], [harga], [jml_brg], [ket], [id]) VALUES (N'handphone', N'android', N'samsing', N'18000000', N'1', N'bonus', N'6')
GO

SET IDENTITY_INSERT [dbo].[tbl_barang] OFF
GO


-- ----------------------------
-- Table structure for tbl_pengajuan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_pengajuan]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_pengajuan]
GO

CREATE TABLE [dbo].[tbl_pengajuan] (
  [no_ajuan] int  NOT NULL,
  [ajuan_id] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [anggota_id] bigint  NOT NULL,
  [tgl_input] datetime2(0)  NOT NULL,
  [jenis] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [nominal] bigint  NOT NULL,
  [lama_ags] int  NOT NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [status] smallint  NOT NULL,
  [alasan] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [tgl_cair] date  NULL,
  [tgl_update] datetime2(0)  NULL,
  [id] int  IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_pengajuan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_pengajuan
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_pengajuan] ON
GO

INSERT INTO [dbo].[tbl_pengajuan] ([no_ajuan], [ajuan_id], [anggota_id], [tgl_input], [jenis], [nominal], [lama_ags], [keterangan], [status], [alasan], [tgl_cair], [tgl_update], [id]) VALUES (N'1', N'B.20.08.001', N'12', N'2020-08-15 22:28:05', N'Biasa', N'100000', N'3', N'tes', N'1', N'mau aja', N'2020-08-15', N'2020-08-15 22:35:00', N'3')
GO

INSERT INTO [dbo].[tbl_pengajuan] ([no_ajuan], [ajuan_id], [anggota_id], [tgl_input], [jenis], [nominal], [lama_ags], [keterangan], [status], [alasan], [tgl_cair], [tgl_update], [id]) VALUES (N'1', N'D.20.08.001', N'13', N'2020-08-15 22:38:17', N'Darurat', N'3000000', N'1', N'coba yahhh', N'2', N'coba aja', NULL, N'2020-08-15 22:39:00', N'4')
GO

SET IDENTITY_INSERT [dbo].[tbl_pengajuan] OFF
GO


-- ----------------------------
-- Table structure for tbl_pinjaman_d
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_pinjaman_d]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_pinjaman_d]
GO

CREATE TABLE [dbo].[tbl_pinjaman_d] (
  [tgl_bayar] datetime2(7)  NULL,
  [pinjam_id] bigint  NULL,
  [angsuran_ke] bigint  NULL,
  [jumlah_bayar] int  NULL,
  [denda_rp] int  NULL,
  [terlambat] int  NULL,
  [ket_bayar] varchar(20) COLLATE Latin1_General_CI_AS  NULL,
  [dk] varchar(20) COLLATE Latin1_General_CI_AS  NULL,
  [kas_id] bigint  NULL,
  [jns_trans] bigint  NULL,
  [update_data] datetime2(0)  NULL,
  [user_name] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NULL,
  [id] bigint  IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_pinjaman_d] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_pinjaman_d
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_pinjaman_d] ON
GO

INSERT INTO [dbo].[tbl_pinjaman_d] ([tgl_bayar], [pinjam_id], [angsuran_ke], [jumlah_bayar], [denda_rp], [terlambat], [ket_bayar], [dk], [kas_id], [jns_trans], [update_data], [user_name], [keterangan], [id]) VALUES (N'2020-08-15 16:26:00.0000000', N'7', N'1', N'851500', N'0', NULL, N'Angsuran', NULL, N'1', N'48', NULL, N'ADMIN', N'bayar pertama', N'1')
GO

INSERT INTO [dbo].[tbl_pinjaman_d] ([tgl_bayar], [pinjam_id], [angsuran_ke], [jumlah_bayar], [denda_rp], [terlambat], [ket_bayar], [dk], [kas_id], [jns_trans], [update_data], [user_name], [keterangan], [id]) VALUES (N'2020-08-15 22:40:00.0000000', N'7', N'2', N'851500', N'0', NULL, N'Angsuran', NULL, N'1', N'48', NULL, N'admin', N'testing', N'3')
GO

SET IDENTITY_INSERT [dbo].[tbl_pinjaman_d] OFF
GO


-- ----------------------------
-- Table structure for tbl_pinjaman_h
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_pinjaman_h]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_pinjaman_h]
GO

CREATE TABLE [dbo].[tbl_pinjaman_h] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [tgl_pinjam] datetime2(0)  NOT NULL,
  [anggota_id] bigint  NOT NULL,
  [barang_id] bigint  NOT NULL,
  [lama_angsuran] bigint  NOT NULL,
  [jumlah] int  NOT NULL,
  [bunga] float(53)  NOT NULL,
  [biaya_adm] int  NOT NULL,
  [lunas] varchar(20) COLLATE Latin1_General_CI_AS  NULL,
  [dk] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [kas_id] bigint  NOT NULL,
  [jns_trans] bigint  NOT NULL,
  [update_data] datetime2(0)  NULL,
  [user_name] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [contoh] int  NULL
)
GO

ALTER TABLE [dbo].[tbl_pinjaman_h] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_pinjaman_h
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_pinjaman_h] ON
GO

INSERT INTO [dbo].[tbl_pinjaman_h] ([id], [tgl_pinjam], [anggota_id], [barang_id], [lama_angsuran], [jumlah], [bunga], [biaya_adm], [lunas], [dk], [kas_id], [jns_trans], [update_data], [user_name], [keterangan], [contoh]) VALUES (N'6', N'2020-08-15 12:52:00', N'1', N'3', N'6', N'100000', N'2', N'1500', NULL, N'K', N'2', N'7', NULL, N'admin', N'tes', NULL)
GO

INSERT INTO [dbo].[tbl_pinjaman_h] ([id], [tgl_pinjam], [anggota_id], [barang_id], [lama_angsuran], [jumlah], [bunga], [biaya_adm], [lunas], [dk], [kas_id], [jns_trans], [update_data], [user_name], [keterangan], [contoh]) VALUES (N'7', N'2020-08-15 16:26:00', N'12', N'2', N'6', N'5000000', N'2', N'1500', N'Belum', N'K', N'1', N'7', NULL, N'ADMIN', N'nyicil komputer', NULL)
GO

SET IDENTITY_INSERT [dbo].[tbl_pinjaman_h] OFF
GO


-- ----------------------------
-- Table structure for tbl_pinjaman_hxxx
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_pinjaman_hxxx]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_pinjaman_hxxx]
GO

CREATE TABLE [dbo].[tbl_pinjaman_hxxx] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [tgl_pinjam] datetime2(0)  NOT NULL,
  [anggota_id] bigint  NOT NULL,
  [barang_id] bigint  NOT NULL,
  [lama_angsuran] bigint  NOT NULL,
  [jumlah] int  NOT NULL,
  [bunga] float(53)  NOT NULL,
  [biaya_adm] int  NOT NULL,
  [lunas] varchar(20) COLLATE Latin1_General_CI_AS  NULL,
  [dk] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [kas_id] bigint  NOT NULL,
  [jns_trans] bigint  NOT NULL,
  [update_data] datetime2(0)  NULL,
  [user_name] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [contoh] int  NULL
)
GO

ALTER TABLE [dbo].[tbl_pinjaman_hxxx] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_pinjaman_hxxx
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_pinjaman_hxxx] ON
GO

INSERT INTO [dbo].[tbl_pinjaman_hxxx] ([id], [tgl_pinjam], [anggota_id], [barang_id], [lama_angsuran], [jumlah], [bunga], [biaya_adm], [lunas], [dk], [kas_id], [jns_trans], [update_data], [user_name], [keterangan], [contoh]) VALUES (N'6', N'2020-08-15 12:52:00', N'1', N'3', N'6', N'100000', N'2', N'1500', NULL, N'K', N'2', N'7', NULL, N'admin', N'tes', NULL)
GO

INSERT INTO [dbo].[tbl_pinjaman_hxxx] ([id], [tgl_pinjam], [anggota_id], [barang_id], [lama_angsuran], [jumlah], [bunga], [biaya_adm], [lunas], [dk], [kas_id], [jns_trans], [update_data], [user_name], [keterangan], [contoh]) VALUES (N'7', N'2020-08-15 16:26:00', N'12', N'2', N'6', N'5000000', N'2', N'1500', N'Belum', N'K', N'1', N'7', NULL, N'ADMIN', N'nyicil komputer', NULL)
GO

SET IDENTITY_INSERT [dbo].[tbl_pinjaman_hxxx] OFF
GO


-- ----------------------------
-- Table structure for tbl_setting
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_setting]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_setting]
GO

CREATE TABLE [dbo].[tbl_setting] (
  [id] bigint  NOT NULL,
  [opsi_key] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [opsi_val] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_setting] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_setting
-- ----------------------------
INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'1', N'nama_lembaga', N'KOPJAM CAKRA BUANA CIREBON')
GO

INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'2', N'nama_ketua', N'ARUEL ARYADINATHA ')
GO

INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'3', N'hp_ketua', N'087123235468')
GO

INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'4', N'alamat', N'Jl. Raya Mundu Kec. Mundu Kab. Cirebon')
GO

INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'5', N'telepon', N'0231-36387985')
GO

INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'6', N'kota', N'Majalengka')
GO

INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'7', N'email', N'cakrabuana_crb@gmail.com')
GO

INSERT INTO [dbo].[tbl_setting] ([id], [opsi_key], [opsi_val]) VALUES (N'8', N'web', N'www.cakrabuana_crb.com')
GO


-- ----------------------------
-- Table structure for tbl_trans_kas
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_trans_kas]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_trans_kas]
GO

CREATE TABLE [dbo].[tbl_trans_kas] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [tgl_catat] datetime2(7)  NOT NULL,
  [jumlah] float(53)  NOT NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [akun] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [dari_kas_id] bigint  NULL,
  [untuk_kas_id] bigint  NULL,
  [jns_trans] bigint  NULL,
  [dk] varchar(10) COLLATE Latin1_General_CI_AS  NULL,
  [update_data] datetime2(7)  NULL,
  [user_name] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_trans_kas] SET (LOCK_ESCALATION = AUTO)
GO


-- ----------------------------
-- Records of tbl_trans_kas
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_trans_kas] ON
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'2', N'2020-08-09 11:23:00.0000000', N'10000', N'keterangan', N'Pengeluaran', N'1', NULL, N'20', N'K', NULL, N'admin')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'3', N'2020-08-09 11:24:00.0000000', N'10000', N'keterangan', N'Transfer', N'1', N'1', N'110', NULL, NULL, N'admin')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'4', N'2020-08-15 12:10:00.0000000', N'1000000', N'tes aaaa', N'Pemasukan', NULL, N'1', N'1', N'D', N'2020-08-15 12:21:00.0000000', N'admin')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'5', N'2020-08-15 12:20:00.0000000', N'1000000', N'tesxxx', N'Pengeluaran', N'1', NULL, N'36', N'K', N'2020-08-15 12:20:00.0000000', N'admin')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'6', N'2020-08-15 12:20:00.0000000', N'100000', N'tes', N'Transfer', N'1', N'1', N'110', NULL, NULL, N'admin')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'7', N'2020-08-15 16:22:00.0000000', N'20000000', N'pemasukan', N'Pemasukan', NULL, N'2', N'1', N'D', NULL, N'ADMIN')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'8', N'2020-08-15 16:23:00.0000000', N'2500000', N'pengeluaran bulanan', N'Pengeluaran', N'2', NULL, N'10', N'K', NULL, N'ADMIN')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'9', N'2020-08-15 16:23:00.0000000', N'340000', N'transfer', N'Transfer', N'1', N'2', N'110', NULL, NULL, N'ADMIN')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'10', N'2020-08-15 22:28:00.0000000', N'5000000', N'testing', N'Pemasukan', NULL, N'7', N'41', N'D', NULL, N'admin')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'11', N'2020-08-15 22:29:00.0000000', N'500000', N'testttt', N'Pengeluaran', N'7', NULL, N'1', N'K', NULL, N'admin')
GO

INSERT INTO [dbo].[tbl_trans_kas] ([id], [tgl_catat], [jumlah], [keterangan], [akun], [dari_kas_id], [untuk_kas_id], [jns_trans], [dk], [update_data], [user_name]) VALUES (N'12', N'2020-08-15 22:30:00.0000000', N'5000000', N'testing', N'Transfer', N'1', N'2', N'110', NULL, NULL, N'admin')
GO

SET IDENTITY_INSERT [dbo].[tbl_trans_kas] OFF
GO


-- ----------------------------
-- Table structure for tbl_trans_sp
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_trans_sp]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_trans_sp]
GO

CREATE TABLE [dbo].[tbl_trans_sp] (
  [id] bigint  IDENTITY(1,1) NOT NULL,
  [tgl_transaksi] datetime2(0)  NOT NULL,
  [anggota_id] bigint  NOT NULL,
  [jenis_id] int  NOT NULL,
  [jumlah] float(53)  NOT NULL,
  [keterangan] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [akun] varchar(20) COLLATE Latin1_General_CI_AS  NOT NULL,
  [dk] varchar(2) COLLATE Latin1_General_CI_AS  NOT NULL,
  [kas_id] bigint  NOT NULL,
  [update_data] datetime2(0)  NULL,
  [user_name] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [nama_penyetor] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [no_identitas] varchar(20) COLLATE Latin1_General_CI_AS  NOT NULL,
  [alamat] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_trans_sp] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_trans_sp
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_trans_sp] ON
GO

INSERT INTO [dbo].[tbl_trans_sp] ([id], [tgl_transaksi], [anggota_id], [jenis_id], [jumlah], [keterangan], [akun], [dk], [kas_id], [update_data], [user_name], [nama_penyetor], [no_identitas], [alamat]) VALUES (N'4', N'2020-08-09 16:04:00', N'1', N'1', N'1000', N'qqq', N'Penarikan', N'K', N'1', NULL, N'admin', N'wewe1`', N'wewe', N'wewe')
GO

INSERT INTO [dbo].[tbl_trans_sp] ([id], [tgl_transaksi], [anggota_id], [jenis_id], [jumlah], [keterangan], [akun], [dk], [kas_id], [update_data], [user_name], [nama_penyetor], [no_identitas], [alamat]) VALUES (N'5', N'2020-08-15 12:21:00', N'1', N'1', N'100000', N'tes aaa', N'Setoran', N'D', N'1', NULL, N'admin', N'alif', N'304955550994', N'surabyaa')
GO

INSERT INTO [dbo].[tbl_trans_sp] ([id], [tgl_transaksi], [anggota_id], [jenis_id], [jumlah], [keterangan], [akun], [dk], [kas_id], [update_data], [user_name], [nama_penyetor], [no_identitas], [alamat]) VALUES (N'6', N'2020-08-15 12:22:00', N'1', N'1', N'220000', N'tteee', N'Setoran', N'D', N'1', NULL, N'admin', N'tes', N'2222', N'rsas')
GO

INSERT INTO [dbo].[tbl_trans_sp] ([id], [tgl_transaksi], [anggota_id], [jenis_id], [jumlah], [keterangan], [akun], [dk], [kas_id], [update_data], [user_name], [nama_penyetor], [no_identitas], [alamat]) VALUES (N'7', N'2020-08-15 16:24:00', N'12', N'2', N'100000', N'simpanan bulanan', N'Setoran', N'D', N'1', NULL, N'ADMIN', N'Rifai', N'352899940044949', N'surabaya')
GO

INSERT INTO [dbo].[tbl_trans_sp] ([id], [tgl_transaksi], [anggota_id], [jenis_id], [jumlah], [keterangan], [akun], [dk], [kas_id], [update_data], [user_name], [nama_penyetor], [no_identitas], [alamat]) VALUES (N'8', N'2020-08-15 16:25:00', N'12', N'2', N'100000', N'penarikan urgent', N'Penarikan', N'K', N'1', NULL, N'ADMIN', N'Ahmad', N'351800339994848', N'urgent')
GO

INSERT INTO [dbo].[tbl_trans_sp] ([id], [tgl_transaksi], [anggota_id], [jenis_id], [jumlah], [keterangan], [akun], [dk], [kas_id], [update_data], [user_name], [nama_penyetor], [no_identitas], [alamat]) VALUES (N'9', N'2020-08-15 22:31:00', N'1', N'2', N'100000', N'testing', N'Setoran', N'D', N'1', NULL, N'admin', N'ario', N'34567845678', N'werty')
GO

INSERT INTO [dbo].[tbl_trans_sp] ([id], [tgl_transaksi], [anggota_id], [jenis_id], [jumlah], [keterangan], [akun], [dk], [kas_id], [update_data], [user_name], [nama_penyetor], [no_identitas], [alamat]) VALUES (N'10', N'2020-08-15 22:31:00', N'12', N'2', N'5000', N'testing', N'Penarikan', N'K', N'7', NULL, N'admin', N'ario', N'12345', N'ererere')
GO

SET IDENTITY_INSERT [dbo].[tbl_trans_sp] OFF
GO


-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_user]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_user]
GO

CREATE TABLE [dbo].[tbl_user] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [u_name] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [pass_word] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [aktif] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL,
  [level] varchar(255) COLLATE Latin1_General_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_user] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_user
-- ----------------------------
SET IDENTITY_INSERT [dbo].[tbl_user] ON
GO

INSERT INTO [dbo].[tbl_user] ([id], [u_name], [pass_word], [aktif], [level]) VALUES (N'4', N'user', N'224bec3dd08832bc6a69873f15a50df406045f40', N'Y', N'operator')
GO

INSERT INTO [dbo].[tbl_user] ([id], [u_name], [pass_word], [aktif], [level]) VALUES (N'5', N'pinjaman', N'efd2770f6782f7218be595baf2fc16bc7cf45143', N'Y', N'pinjaman')
GO

INSERT INTO [dbo].[tbl_user] ([id], [u_name], [pass_word], [aktif], [level]) VALUES (N'1', N'admin', N'c915d22bdd8fb8efe65269537d27e77b7df1b407', N'Y', N'admin')
GO

INSERT INTO [dbo].[tbl_user] ([id], [u_name], [pass_word], [aktif], [level]) VALUES (N'6', N'ario', N'c915d22bdd8fb8efe65269537d27e77b7df1b407', N'Y', N'pinjaman')
GO

INSERT INTO [dbo].[tbl_user] ([id], [u_name], [pass_word], [aktif], [level]) VALUES (N'9', N'sule', N'1e157dd5081c6699192c94068932336f5c00ebf5', N'Y', N'admin')
GO

INSERT INTO [dbo].[tbl_user] ([id], [u_name], [pass_word], [aktif], [level]) VALUES (N'10', N'ahmad', N'8ef74032faf83d0c2caf3022418196a1e0527972', N'Y', N'operator')
GO

SET IDENTITY_INSERT [dbo].[tbl_user] OFF
GO


-- ----------------------------
-- View structure for v_hitung_pinjaman
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[v_hitung_pinjaman]') AND type IN ('V'))
	DROP VIEW [dbo].[v_hitung_pinjaman]
GO

CREATE VIEW [dbo].[v_hitung_pinjaman] AS SELECT
  tbl_pinjaman_h.id            AS id,
  tbl_pinjaman_h.tgl_pinjam    AS tgl_pinjam,
  tbl_pinjaman_h.anggota_id    AS anggota_id,
  tbl_pinjaman_h.lama_angsuran AS lama_angsuran,
  tbl_pinjaman_h.jumlah        AS jumlah,
  tbl_pinjaman_h.bunga         AS bunga,
  tbl_pinjaman_h.biaya_adm     AS biaya_adm,
  tbl_pinjaman_h.lunas         AS lunas,
  tbl_pinjaman_h.dk            AS dk,
  tbl_pinjaman_h.kas_id        AS kas_id,
  tbl_pinjaman_h.user_name     AS user_name,
	(tbl_pinjaman_h.jumlah / tbl_pinjaman_h.lama_angsuran) AS pokok_angsuran,
	ROUND(CEILING((((tbl_pinjaman_h.jumlah / tbl_pinjaman_h.lama_angsuran) * tbl_pinjaman_h.bunga) / 100)),-(2)) AS bunga_pinjaman,
	ROUND(CEILING((((((((tbl_pinjaman_h.jumlah / tbl_pinjaman_h.lama_angsuran) * tbl_pinjaman_h.bunga) / 100) + (tbl_pinjaman_h.jumlah / tbl_pinjaman_h.lama_angsuran)) + tbl_pinjaman_h.biaya_adm) * 100) / 100)),-(2)) AS ags_per_bulan,
	DATEADD(month, tbl_pinjaman_h.lama_angsuran, tbl_pinjaman_h.tgl_pinjam) AS tempo,
	(ROUND(CEILING((((((((tbl_pinjaman_h.jumlah / tbl_pinjaman_h.lama_angsuran) * tbl_pinjaman_h.bunga) / 100) + (tbl_pinjaman_h.jumlah / tbl_pinjaman_h.lama_angsuran)) + tbl_pinjaman_h.biaya_adm) * 100) / 100)),-(2)) * tbl_pinjaman_h.lama_angsuran) AS tagihan,
	tbl_pinjaman_h.keterangan    AS keterangan,
  tbl_pinjaman_h.barang_id     AS barang_id,
	CASE WHEN MAX(tbl_pinjaman_d.angsuran_ke) IS NULL THEN 0 ELSE NULL END AS bln_sudah_angsur
FROM
	tbl_pinjaman_h
	LEFT JOIN tbl_pinjaman_d on tbl_pinjaman_h.id = tbl_pinjaman_d.pinjam_id
GROUP BY 
	dbo.tbl_pinjaman_h.id,
	tbl_pinjaman_h.tgl_pinjam,
	tbl_pinjaman_h.anggota_id,
	tbl_pinjaman_h.lama_angsuran,
	tbl_pinjaman_h.jumlah,
	tbl_pinjaman_h.bunga,
	tbl_pinjaman_h.biaya_adm,
	tbl_pinjaman_h.lunas,
	tbl_pinjaman_h.dk,
	tbl_pinjaman_h.kas_id,
	tbl_pinjaman_h.user_name,
	tbl_pinjaman_h.keterangan,
	tbl_pinjaman_h.barang_id
GO


-- ----------------------------
-- View structure for v_transaksi
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[v_transaksi]') AND type IN ('V'))
	DROP VIEW [dbo].[v_transaksi]
GO

CREATE VIEW [dbo].[v_transaksi] AS SELECT
	'A' AS tbl,
	tbl_pinjaman_h.id AS id,
	tbl_pinjaman_h.tgl_pinjam AS tgl,
	tbl_pinjaman_h.jumlah AS kredit,
	0 AS debet,
	tbl_pinjaman_h.kas_id AS dari_kas,
	NULL AS untuk_kas,
	tbl_pinjaman_h.jns_trans AS transaksi,
	tbl_pinjaman_h.keterangan AS ket,
	tbl_pinjaman_h.user_name AS 'user' 
FROM
	tbl_pinjaman_h UNION
SELECT
	'B' AS tbl,
	tbl_pinjaman_d.id AS id,
	tbl_pinjaman_d.tgl_bayar AS tgl,
	0 AS kredit,
	tbl_pinjaman_d.jumlah_bayar AS debet,
	NULL AS dari_kas,
	tbl_pinjaman_d.kas_id AS untuk_kas,
	tbl_pinjaman_d.jns_trans AS transaksi,
	tbl_pinjaman_d.keterangan AS ket,
	tbl_pinjaman_d.user_name AS 'user' 
FROM
	tbl_pinjaman_d UNION
SELECT
	'C' AS tbl,
	tbl_trans_sp.id AS id,
	tbl_trans_sp.tgl_transaksi AS tgl,
CASE
		WHEN ( tbl_trans_sp.dk = 'K' ) THEN
		tbl_trans_sp.jumlah ELSE 0 
	END AS kredit,
CASE
		WHEN ( tbl_trans_sp.dk = 'D' ) THEN
		tbl_trans_sp.jumlah ELSE 0 
	END AS debet,
CASE
		WHEN ( tbl_trans_sp.dk = 'K' ) THEN
		tbl_trans_sp.kas_id ELSE NULL 
	END AS dari_kas,
CASE
		WHEN ( tbl_trans_sp.dk = 'D' ) THEN
		tbl_trans_sp.kas_id ELSE NULL 
	END AS untuk_kas,
	tbl_trans_sp.jenis_id AS transaksi,
	tbl_trans_sp.keterangan AS ket,
	tbl_trans_sp.user_name AS 'user' 
FROM
	tbl_trans_sp UNION
SELECT
	'D' AS tbl,
	tbl_trans_kas.id AS id,
	tbl_trans_kas.tgl_catat AS tgl,
CASE
		
		WHEN ( tbl_trans_kas.dk = 'K' ) THEN
		tbl_trans_kas.jumlah ELSE
	CASE
			
			WHEN ( tbl_trans_kas.dk IS NULL ) THEN
			tbl_trans_kas.jumlah ELSE 0 
		END 
		END AS kredit,
	CASE
			
			WHEN ( tbl_trans_kas.dk = 'D' ) THEN
			tbl_trans_kas.jumlah ELSE
		CASE
				
				WHEN ( tbl_trans_kas.dk IS NULL ) THEN
				tbl_trans_kas.jumlah ELSE 0 
			END 
			END AS debet,
			tbl_trans_kas.dari_kas_id AS dari_kas,
			tbl_trans_kas.untuk_kas_id AS untuk_kas,
			tbl_trans_kas.jns_trans AS transaksi,
			tbl_trans_kas.keterangan AS ket,
			tbl_trans_kas.user_name AS 'user' 
		FROM
			tbl_trans_kas
GO


-- ----------------------------
-- Auto increment value for jns_akun
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jns_akun]', RESEED, 41)
GO


-- ----------------------------
-- Auto increment value for jns_angsuran
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jns_angsuran]', RESEED, 8)
GO


-- ----------------------------
-- Auto increment value for jns_simpan
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jns_simpan]', RESEED, 17)
GO


-- ----------------------------
-- Primary Key structure for table jns_simpan
-- ----------------------------
ALTER TABLE [dbo].[jns_simpan] ADD CONSTRAINT [PK__jns_simp__3213E83FC6314E22] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for nama_kas_tbl
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[nama_kas_tbl]', RESEED, 7)
GO


-- ----------------------------
-- Primary Key structure for table nama_kas_tbl
-- ----------------------------
ALTER TABLE [dbo].[nama_kas_tbl] ADD CONSTRAINT [PK__nama_kas__3213E83FD395AA6F] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for suku_bunga
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[suku_bunga]', RESEED, 15)
GO


-- ----------------------------
-- Primary Key structure for table suku_bunga
-- ----------------------------
ALTER TABLE [dbo].[suku_bunga] ADD CONSTRAINT [PK__suku_bun__3213E83FAC18518C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_anggota
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_anggota]', RESEED, 13)
GO


-- ----------------------------
-- Uniques structure for table tbl_anggota
-- ----------------------------
ALTER TABLE [dbo].[tbl_anggota] ADD CONSTRAINT [identitas] UNIQUE NONCLUSTERED ([identitas] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table tbl_anggota
-- ----------------------------
ALTER TABLE [dbo].[tbl_anggota] ADD CONSTRAINT [PK__tbl_angg__3213E83F26B15C75] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_barang
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_barang]', RESEED, 6)
GO


-- ----------------------------
-- Primary Key structure for table tbl_barang
-- ----------------------------
ALTER TABLE [dbo].[tbl_barang] ADD CONSTRAINT [PK__tbl_bara__3213E83F6D1190B9] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_pengajuan
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_pengajuan]', RESEED, 4)
GO


-- ----------------------------
-- Primary Key structure for table tbl_pengajuan
-- ----------------------------
ALTER TABLE [dbo].[tbl_pengajuan] ADD CONSTRAINT [PK__tbl_peng__DC501A3EBAA90E58] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_pinjaman_d
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_pinjaman_d]', RESEED, 3)
GO


-- ----------------------------
-- Primary Key structure for table tbl_pinjaman_d
-- ----------------------------
ALTER TABLE [dbo].[tbl_pinjaman_d] ADD CONSTRAINT [PK__tbl_pinj__3213E83F0AC01D0C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_pinjaman_h
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_pinjaman_h]', RESEED, 7)
GO


-- ----------------------------
-- Primary Key structure for table tbl_pinjaman_h
-- ----------------------------
ALTER TABLE [dbo].[tbl_pinjaman_h] ADD CONSTRAINT [PK__tbl_pinj__3213E83F94CD699E_copy2] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_pinjaman_hxxx
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_pinjaman_hxxx]', RESEED, 7)
GO


-- ----------------------------
-- Primary Key structure for table tbl_pinjaman_hxxx
-- ----------------------------
ALTER TABLE [dbo].[tbl_pinjaman_hxxx] ADD CONSTRAINT [PK__tbl_pinj__3213E83F94CD699E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table tbl_setting
-- ----------------------------
ALTER TABLE [dbo].[tbl_setting] ADD CONSTRAINT [PK__tbl_sett__3213E83F0C379B5D] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_trans_kas
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_trans_kas]', RESEED, 12)
GO


-- ----------------------------
-- Indexes structure for table tbl_trans_kas
-- ----------------------------
CREATE NONCLUSTERED INDEX [tbl_trans_kas_id]
ON [dbo].[tbl_trans_kas] (
  [id] ASC
)
GO


-- ----------------------------
-- Primary Key structure for table tbl_trans_kas
-- ----------------------------
ALTER TABLE [dbo].[tbl_trans_kas] ADD CONSTRAINT [PK__tbl_tran__3213E83FB1087F81] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_trans_sp
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_trans_sp]', RESEED, 10)
GO


-- ----------------------------
-- Indexes structure for table tbl_trans_sp
-- ----------------------------
CREATE NONCLUSTERED INDEX [anggota_id]
ON [dbo].[tbl_trans_sp] (
  [anggota_id] ASC
)
GO

CREATE NONCLUSTERED INDEX [jenis_id]
ON [dbo].[tbl_trans_sp] (
  [jenis_id] ASC
)
GO

CREATE NONCLUSTERED INDEX [kas_id]
ON [dbo].[tbl_trans_sp] (
  [kas_id] ASC
)
GO

CREATE NONCLUSTERED INDEX [user_name]
ON [dbo].[tbl_trans_sp] (
  [user_name] ASC
)
GO


-- ----------------------------
-- Primary Key structure for table tbl_trans_sp
-- ----------------------------
ALTER TABLE [dbo].[tbl_trans_sp] ADD CONSTRAINT [PK__tbl_tran__3213E83F925E70D6] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for tbl_user
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[tbl_user]', RESEED, 10)
GO


-- ----------------------------
-- Checks structure for table tbl_user
-- ----------------------------
ALTER TABLE [dbo].[tbl_user] ADD CONSTRAINT [tbl_user_aktif] CHECK ([aktif]='N' OR [aktif]='Y')
GO

ALTER TABLE [dbo].[tbl_user] ADD CONSTRAINT [tbl_user_level] CHECK ([level]='pinjaman' OR [level]='operator' OR [level]='admin')
GO


-- ----------------------------
-- Primary Key structure for table tbl_user
-- ----------------------------
ALTER TABLE [dbo].[tbl_user] ADD CONSTRAINT [PK_tbl_user] PRIMARY KEY NONCLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Foreign Keys structure for table tbl_pinjaman_d
-- ----------------------------
ALTER TABLE [dbo].[tbl_pinjaman_d] ADD CONSTRAINT [tbl_pinjaman_d_pinjam_id] FOREIGN KEY ([pinjam_id]) REFERENCES [dbo].[tbl_pinjaman_hxxx] ([id]) ON DELETE CASCADE ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[tbl_pinjaman_d] ADD CONSTRAINT [tbl_pinjaman_d_kas_id] FOREIGN KEY ([kas_id]) REFERENCES [dbo].[nama_kas_tbl] ([id]) ON DELETE NO ACTION ON UPDATE CASCADE
GO


-- ----------------------------
-- Foreign Keys structure for table tbl_trans_sp
-- ----------------------------
ALTER TABLE [dbo].[tbl_trans_sp] ADD CONSTRAINT [tbl_trans_sp_ibfk_1] FOREIGN KEY ([anggota_id]) REFERENCES [dbo].[tbl_anggota] ([id]) ON DELETE NO ACTION ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[tbl_trans_sp] ADD CONSTRAINT [tbl_trans_sp_ibfk_2] FOREIGN KEY ([kas_id]) REFERENCES [dbo].[nama_kas_tbl] ([id]) ON DELETE NO ACTION ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[tbl_trans_sp] ADD CONSTRAINT [tbl_trans_sp_ibfk_4] FOREIGN KEY ([jenis_id]) REFERENCES [dbo].[jns_simpan] ([id]) ON DELETE NO ACTION ON UPDATE CASCADE
GO

